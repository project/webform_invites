
-- SUMMARY --

Webform invites is module that adds feature of protecting specific webform survey 
by inviting users by email and sending out special access code for accessing it 
without registration on drupal site. It also denies filling form twice with the 
same access code.


-- REQUIREMENTS --

webform module


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Customize module settings in Administer >> Site configuration >> Webform Invites.


-- FAQ --

Q: Why is hidden field "%session[webform_invites_secretkey]" needed?

A: This field is saved when filling out form. If user with the same access code 
   tries to access webform again, it gets rejected.


-- CONTACT --

Current maintainers:
* Janis Bebritis (Jancis) - jancis@iists.it

This project has been sponsored by:
* PROG (www.prog.lv)
  Specialized in consulting and planning of Drupal powered sites, PROG offers 
  installation, development, theming, customization, and hosting
  to get you started. Visit http://www.prog.lv for more information.

